HLAA - homework "sandsail"

*Player can select behavior in orders menu (the icon representing it was created specifically for this task),
*Behavior moves only battle commander.

Bonus criteria:
*unit with the behavior assigned will not get stuck on various obstacles (like edges of the map). While the wind blows outside the map the unit will stand, but as soon as the wind changes direction to the traversable terrain, the unit will move.